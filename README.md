I retrain the script with reward value 0.1 and 0.5.
I did not change the configuration in keras.json so it will use Tensorflow or Theano backend
I train the script with the following cofiguration
    
    gpu=geforce 920mx
    CNMeM disable
    cuDNN=5005
    
I run the program and observe the behaviour of the bird for both reward value 0.1 and 0.5.
For some reason, the running script got stuck at 12000th timestep. This happened for both reward value 0.1 and 0.5.
There was no error when I'm running the script so this problem could possibly happened because of two reason.

    1: My system is not good enough
    2: Complication when installing Cuda and cuDNN
    
I had already reinstalling back my operating system and all dependencies required to run the script and still unable to avoid
to get stuck at 12000th timestep.

Below are my system

    Ubuntu 16.04 LTS
    NVIDIA Geforce 920MX 2GB
    4GB DDR3
    CUDA 7.5,18
    cuDNN version 5